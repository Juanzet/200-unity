using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{

    public static LevelGenerator instance { get; private set; }
    [SerializeField] int minElements;
    [SerializeField] int maxElements;

    // esto sirve para generar un espacio entre cada instancia de gameobjects
    float dbaY = 0.6f; //distancia entre ejez sobre Y
    float dbaX = 1.1f; // distancia entre ejez sobre X
    
    //cantidad de filas en eje Y e X
    [SerializeField] float rowsY;
    [SerializeField] float rowsX;

    //[SerializeField] GameObject block;
    [SerializeField] GameObject[] blocks;

    int blocksInGame;
 
     void Awake() 
    {
        if(instance != null && instance != this)
        {
            Destroy(gameObject);
        } 
        else 
        { 
            //DontDestroyOnLoad(gameObject);
            instance = this;
        }
    }

    void Start()
    {
        InstantiateLevel();
    }
 
    void Update() 
    {
        Debug.Log("cantidad prefabs " + blocksInGame);  
    }

    public void TotalBlocks(int b)
    {
        b = GameManager.Instance.blocksLeft;
        
        if(blocksInGame <= b)
        {
            GameManager.Instance.blocksLeft = 0;
            b = 0;
            DeleteBoard();
            InstantiateLevel();
           //GameManager.Instance.ReloadLevel();
        }  
    }
    public void InstantiateLevel()
    {
        // solucione el tema de los espacios entre cada bloque, tenia que subir un punto al flotante X que determina el final del bucle for y poner 1f como resultado final
        //(0.75f = 12, 1.2f = 8, 0.95f = 10) tengo que solucionar el problema que me esta dando mucho espacio entre cada instancia 
        for (float y = rowsY; y > -rowsY; y -= dbaY)
        {
            for (float x = -rowsX; x < rowsX; x += dbaX)
            {
                // para ajustar el random y que no de error hay que agregar uno de mas
                //eso quiere decir que si queremos agregar 2 elementos es de 0 a 2 no de 0 a 1
               int randomPrefab = Random.Range(minElements, maxElements); 

                //Instantiate(block, new Vector2(x, y), block.transform.rotation);
                Instantiate(blocks[randomPrefab], new Vector2(x,y), Quaternion.identity);
            }
        }
        blocksInGame = GameObject.FindGameObjectsWithTag("Block").Length;

    }

    public void DeleteBoard() 
    {
        for (int i = 0; i < blocks.Length; i++)
        {
            Destroy(blocks[i]);
        }
        /* foreach para destruir objetos NO FUNCIONO
       foreach (GameObject g in blocks)
       {
            Destroy(g);
       }
       */
        /* for para destruir objetos NO FUNCIONO
        for (int i = 0; i < blocks.Length; i++)   
        {
            Destroy(blocks[i]);
        }
        */
    }
}
