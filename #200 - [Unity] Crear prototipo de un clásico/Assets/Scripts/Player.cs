using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    [SerializeField] float speed;
    //float bounds = 4f; 
   #region PowerUpBullet
    bool isPistolActive = false;
    [SerializeField] GameObject bullet;
    [SerializeField] Transform pointOfShoot;
    #endregion
 
    void Update()
    {
        if(Input.GetKey(KeyCode.Escape))
            SceneManager.LoadScene("MainMenu");

        
        if(Input.GetKey(KeyCode.R))
            SceneManager.LoadScene("Level1");   
        
        
        MoveLogic();
        PistolActive();
    }

    void MoveLogic() 
    {
        float xInput = Input.GetAxisRaw("Horizontal");

        // lo que hice fue crear un vector2 que guarde la posicion del jugador y con el metodo Clamp de la clase MathF determinar una distancia de movimiento en el eje X 
        Vector2 playerMoveX = transform.position; // guardar posicion
        playerMoveX.x = Mathf.Clamp(transform.position.x + xInput * speed * Time.deltaTime, -4.34f, 4.18f); // limitar distancia
        transform.position = playerMoveX;
    }

    void PistolActive()
    {
        if(isPistolActive == true)
        {
            if(Input.GetButtonDown("Fire1"))
            {
                //Disparar
                Instantiate(bullet, pointOfShoot.position, pointOfShoot.rotation);
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.gameObject.CompareTag("PU"))
        {
            Destroy(other.gameObject);
            isPistolActive = true;
        }
   
    }
}
