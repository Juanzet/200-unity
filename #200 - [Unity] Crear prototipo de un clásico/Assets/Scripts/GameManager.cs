using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    [SerializeField] TMP_Text points;
    // estoy creando un singleton, esto hace que sea globalmente accesible y que solo haya una por escena
    //esto nos permite manejar el loop del juego desde esta clase bajando el acoplamiento y generando alta modularidad
    public static GameManager Instance { get; private set; }
   [HideInInspector] public int blocksLeft;
    int pointsBlock;

    void Awake() 
    {
        if(Instance != null && Instance != this)
        {
            Destroy(gameObject);
        } 
        else 
        {
           //DontDestroyOnLoad(gameObject);
           Instance = this;
        }
    }

    void Start()
    {
       points.text = GameObject.Find("Points").ToString();
    }

    void Update() 
    {
        NextLevel();
       //Debug.Log("Blocks counter: " + blocksCounter);
       //Debug.Log("Blocks left: " + blocksLeft);
        points.text = $"Puntos: {pointsBlock}";    
    }

    void NextLevel()
    {
        //blocksLeft = 0;
        LevelGenerator.instance.DeleteBoard();
        LevelGenerator.instance.TotalBlocks(blocksLeft);
    }

    public void BlockDestroyed()
    {
        blocksLeft++;
        pointsBlock++;
    }

    public void ReloadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        //LevelGenerator.instance.InstantiateBoard();
    }
}
