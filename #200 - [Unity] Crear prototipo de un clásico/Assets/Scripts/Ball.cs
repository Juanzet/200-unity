using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField] Vector2 launchVelocity;
    bool isMoving = false;
    Rigidbody2D rb;
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    
    void Update()
    {
        MoveLogicBall();
    }

    void MoveLogicBall()
    {
        if(Input.GetKey(KeyCode.Space) && !isMoving)
        {
            rb.velocity = launchVelocity;
            transform.parent = null;
            isMoving = true;
        }
    }

    void FixBall() // este metodo es pura y exclusivamente para corregir un error donde la pelota iba en line recta de un punto a otro
    {
        float velocityD = 0.5f;
        float minVelocity = 0.2f;

        if(!isMoving)
        {
            if(Mathf.Abs(rb.velocity.x) < minVelocity)
            {
                velocityD = Random.value < 0.5f ? velocityD : -velocityD;
                rb.velocity += new Vector2(velocityD, 0f);
            }

            if(Mathf.Abs(rb.velocity.y) < minVelocity) 
            {
                velocityD = Random.value < 0.5f ? velocityD : -velocityD;
                rb.velocity += new Vector2(0f, velocityD);
            }
        }
        
    }

    private void OnCollisionEnter2D(Collision2D other) 
    {
        if(other.gameObject.CompareTag("Block"))
        {
            FixBall();
            SoundManager.instance.Play("PingPong");
            //GameObject.Find("GameManager").GetComponent<GameManager>().BlockDestroyed();
            GameManager.Instance.BlockDestroyed();
            Destroy(other.gameObject);
        }

        if(other.gameObject.CompareTag("Wall"))
            FixBall();
            
        if(other.gameObject.CompareTag("YellowBlock")) 
        {
            SoundManager.instance.Play("Irrompible");
            FixBall();
        } 

        if(other.gameObject.CompareTag("Player"))
            SoundManager.instance.Play("Pong");
    }
}
