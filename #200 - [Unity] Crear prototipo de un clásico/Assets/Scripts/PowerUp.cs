using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using System;

public class PowerUp : MonoBehaviour
{
    #region variables de estetica
    public SpriteRenderer sprite;
    public Color[] colors;
    int currentColorI = 0;
    int targetColorI = 1;
    float targetPoint;
    public float time;
    #endregion

    [SerializeField] GameObject[] powerUp;

    void Update()
    {
        ColorLogic();
    }

    void ColorLogic() 
    {
        targetPoint += Time.deltaTime;
        sprite.color = Color.Lerp(colors[currentColorI], colors[targetColorI], targetPoint);
        if(targetPoint >= 1f)
        {
            targetPoint = 0f;
            currentColorI = targetColorI;
            targetColorI++;

            if(targetColorI == colors.Length)
                targetColorI = 0;
        }
    }

    void PowerUpSelection()
    {
       int rand = UnityEngine.Random.Range(0,2);
       Instantiate(powerUp[rand], transform.position, Quaternion.identity);
    }

    private void OnCollisionEnter2D(Collision2D other) 
    {   
        if(other.gameObject.CompareTag("Ball"))
            PowerUpSelection();
    }
}
