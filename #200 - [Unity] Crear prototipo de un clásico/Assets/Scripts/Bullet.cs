using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] float vel;
    [SerializeField] GameObject deathEffect;
    //[SerializeField] float dmg;
    void Start() 
    {
        Destroy(this,3f);   
    }
    
    void Update()
    {
        transform.Translate(Vector2.up * vel * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.gameObject.CompareTag("Block")) 
        {
            Instantiate(deathEffect,transform.position, transform.rotation);
            Destroy(other.gameObject);
            Destroy(this.gameObject, 0.1f);
            GameManager.Instance.BlockDestroyed();
        }   

        if(other.gameObject.CompareTag("YellowBlock"))
        {
            Instantiate(deathEffect,transform.position, transform.rotation);
            Destroy(other.gameObject);
            Destroy(this.gameObject, 0.1f);
            //GameManager.Instance.BlockDestroyed();
        }
    }
}
