using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoseTrigger : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.gameObject.CompareTag("Ball")) 
        {
            SoundManager.instance.Play("Dead");
            GameManager.Instance.ReloadLevel();
        }

             
    }
}
