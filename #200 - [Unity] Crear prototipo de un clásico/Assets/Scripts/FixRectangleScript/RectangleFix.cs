using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RectangleFix : MonoBehaviour
{
    /// <summary>
    /// Estoy teniendo un error que hace que otros rectangulos amarillos no se borren con el metodo  <paramref name="LevelGenerator.Instance.DeleteBoard"/>
    /// La solucion rapida era crear este script que hace quee al entrar en contacto con este tipo de rectangulo lo borre
    /// para evitar que se superpongan 
    /// Voy a seguir fixeando este Script 
    /// <paramref name="LevelGenerator>
    /// </summary>
    /// <param name="other"></param>
    private void OnCollisionEnter2D(Collision2D other) 
    {
        if(other.gameObject.CompareTag("YellowBlock"))
        {
            Destroy(other.gameObject);
        }    
    }
}
